package ru.vsu.hospitality.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Entry point for HOS application.
 *
 * @author Alexey Alimov
 */
@SpringBootApplication(scanBasePackages = "ru.vsu.hospitality")
public class HosApp {

  /**
   * Application entry point.
   */
  public static void main(final String[] args) {
    SpringApplication.run(HosApp.class);
  }
}
