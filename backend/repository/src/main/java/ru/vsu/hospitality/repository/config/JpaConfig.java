package ru.vsu.hospitality.repository.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Class for configuring Spring JPA.
 *
 * @author Alexey Alimov
 */
@Configuration
@EntityScan("ru.vsu.hospitality.repository.entity")
@EnableJpaRepositories(basePackages = "ru.vsu.hospitality.repository.repository")
@EnableTransactionManagement
@PropertySource("application.properties")
public class JpaConfig {

}

